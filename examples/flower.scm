(use-modules (vic canvas))

(define pi 3.14159265358979323846)
(define units-per-ms (/ internal-time-units-per-second 1000))

(draw (with-save
       (set-operator 'source)
       (set-source-rgba 0 0 0 0)
       (scale 200 200)
       (paint)

       (set-operator 'over)
       (translate .5 .5)
       (rotate (* (/ (- pi) 180)
                  (/ (get-internal-run-time) units-per-ms)
                  0.36) )

       (with-save
        (translate -.5 -.5)
        (move-to .5 .4)
        (curve-to 0.5 0.2 0.6 0.1 0.7 0.1)
        (curve-to 0.8 0.1 0.9 0.2 0.9 0.3)
        (curve-to 0.9 0.4 0.8 0.5 0.6 0.5)
        (curve-to 0.8 0.5 0.9 0.6 0.9 0.7)
        (curve-to 0.9 0.8 0.8 0.9 0.7 0.9)
        (curve-to 0.6 0.9 0.5 0.8 0.5 0.6)
        (curve-to 0.5 0.8 0.4 0.9 0.3 0.9)
        (curve-to 0.2 0.9 0.1 0.8 0.1 0.7)
        (curve-to 0.1 0.6 0.2 0.5 0.3 0.5)
        (curve-to 0.2 0.5 0.1 0.4 0.1 0.3)
        (curve-to 0.1 0.2 0.2 0.1 0.3 0.1)
        (curve-to 0.4 0.1 0.5 0.2 0.5 0.3))

       (set-source-rgba 1 0.6 0.6 1)
       (fill)))
