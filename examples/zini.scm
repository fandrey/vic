(use-modules (vic canvas))

(define pi 3.14159265358979323846)
(define max 75)

(let ((counter 0))
  (draw (with-save
         (define size (canvas-width))
         (define len (/ size 25))

         (set! counter (modulo (+ counter 4) 1000))

         ;; (scale 1 1)
         ;; (set-operator 'over)
         ;; (set-source-rgb 1.0 0.5 0.25)
         ;; (paint)

         (set-line-cap 'round)
         (set-operator 'over)
         (set-line-width len)

         (do ((i 0 (1+ i)))
             ((>= i max))
           (with-save
            (translate (/ size 2) (/ size 2))
            (rotate (* (/ pi 180) (+ counter (* 10 i)) 0.36))
            (translate 0 (+ (* size 0.33)
                            (* size 0.0825
                               (sin (* (/ (+ counter (* 10 i)) 1000) 10 pi)))))
            (rotate (* (/ pi 180) 6 i))
            (set-source-rgba 1 (/ i max) (- 1 (/ i max)) (* i 0.01))
            (move-to (- len) 0)
            (line-to len 0)
            (stroke))))))
