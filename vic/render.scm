;;; Copyright (C) 2020 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of VIC.
;;;
;;; VIC is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; VIC is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with VIC.  If not, see <http://www.gnu.org/licenses/>.

(define-module (vic render)
  #:use-module (ice-9 atomic)
  #:use-module (ice-9 receive)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:use-module (gl)
  #:use-module (vic gl)
  #:use-module (glfw)
  #:use-module (cairo))

(current-gl-resolver (lambda (name)
                       (dynamic-link "libGL")
                       (current-gl-resolver get-proc-address)
                       (get-proc-address name)))

(define (compile-shader type shader)
  "Compile SHADER of TYPE"
  (let ((sh (gl-create-shader type)))
    (gl-shader-source sh shader)
    (gl-compile-shader sh)
    (unless (gl-get-shader sh compile-status)
      (gl-delete-shader sh)
      (error "shader compilation failed: ~%~A"
             (gl-get-shader-info-log sh)))
    sh))

(define (free-shader-program program)
  (map (lambda (sh)
         (with-check-gl-error (gl-delete-shader sh)))
       (with-check-gl-error (gl-get-attached-shaders program)))
  (with-check-gl-error (gl-delete-program program)))

;;; emacs: (put 'with-cleanup 'scheme-indent-function 0)
(define-syntax-rule (with-cleanup thunk cleanup)
  (let ((cleanup-proc (lambda () cleanup)))
    (dynamic-wind
      (const #t)
      (lambda ()
        (let ((value thunk))
          (set! cleanup-proc (const #t))
          value))
      (lambda ()
        (cleanup-proc)))))

(define (compile-and-attach program type shader)
  (let ((sh (compile-shader type shader)))
    (with-cleanup
      (with-check-gl-error (gl-attach-shader program sh))
      (gl-delete-shader sh))))

(define (make-shader-program vertex fragment)
  (let ((pr (gl-create-program)))
    (if (= pr 0)
        (error "failed to create a program"))
    (with-cleanup
      (begin
        (compile-and-attach pr 'vertex vertex)
        (compile-and-attach pr 'fragment fragment)
        (gl-link-program pr)
        (unless (gl-get-program pr link-status)
          (error "program linking failed: ~%~A"
                 (gl-get-program-info-log pr))))
      (free-shader-program pr))
    pr))

(define (load-shaders vertex-shader fragment-shader . attrib-locations)
  (let ((program (make-shader-program vertex-shader fragment-shader)))
    (apply values (cons program
                        (with-cleanup
                          (map (lambda (attr)
                                 (with-check-gl-error
                                  (gl-get-attrib-location program attr)))
                               attrib-locations)
                          (free-shader-program program))))))

(define-public (init-render)
  (receive (program position texposition)
      (load-shaders "attribute vec2 position;
                     attribute vec2 texposition;
                     uniform mat3 view;
                     varying vec2 texcoord;
                     void main()
                     {
                       gl_Position = vec4(view * vec3(position, 0.), 1.);
                       texcoord = texposition;
                     }"
                    "varying vec2 texcoord;
                     uniform sampler2D sampler;
                     void main()
                     {
                       gl_FragColor = texture2D(sampler, texcoord);
                     }"
                    "position" "texposition")
    (let* ((view (gl-get-uniform-location program "view"))
           (vertex-array 0)
           (vertex-buffer 0)
           (texture 0))
      (define (close-render)
        (when program
          (free-shader-program program)
          (set! program #f))
        (unless (zero? vertex-buffer)
          (gl-delete-buffer vertex-buffer)
          (set! vertex-buffer 0))
        (unless (zero? vertex-array)
          (gl-delete-vertex-arrays (list vertex-array))
          (set! vertex-array 0))
        (unless (zero? texture)
          (gl-delete-textures (list texture))
          (set! texture 0)))
      (define (load-texture rgba w h)
        (let ((target (texture-target texture-2d)))
          (gl-bind-texture target texture)
          (gl-tex-parameter target
                            (texture-parameter-name texture-mag-filter)
                            (texture-mag-filter linear))
          (gl-tex-parameter target
                            (texture-parameter-name texture-min-filter)
                            (texture-mag-filter linear))
          (gl-tex-image-2d target 0
                           (pixel-internal-format rgba8)
                           w h 0
                           gl-bgra
                           (data-type unsigned-byte)
                           rgba)))
      (define (render window)
        (gl-clear (clear-buffer-mask color-buffer))
        (gl-use-program program)
        (gl-uniform-matrix-3fv view 1 (boolean false)
                               (f32vector 1f0 0f0 0f0
                                          0f0 1f0 0f0
                                          0f0 0f0 1f0))
        (gl-bind-vertex-array vertex-array)
        (gl-bind-buffer (arb-vertex-buffer-object array-buffer-arb)
                        vertex-buffer)
        (gl-bind-texture (texture-target texture-2d) texture)
        (gl-draw-arrays (begin-mode triangle-strip) 0 4)
        (swap-buffers window))
      (with-cleanup
        (let ((bv (make-bytevector (* (sizeof float) 4 2 2)))
              (target (arb-vertex-buffer-object array-buffer-arb)))
          (set! vertex-array (car (gl-gen-vertex-arrays 1)))
          (when (zero? vertex-array)
            (error "failed to generate a vertex array"))
          (set! vertex-buffer (gl-generate-buffer))
          (when (zero? vertex-buffer)
            (error "failed to generate a vertex buffer"))
          (set! texture (gl-generate-texture))
          (when (zero? texture)
            (error "failed to generate a texture"))
          (f32vector-set! bv  0 -1f0)
          (f32vector-set! bv  1 -1f0)
          (f32vector-set! bv  2  1f0)
          (f32vector-set! bv  3 -1f0)
          (f32vector-set! bv  4 -1f0)
          (f32vector-set! bv  5  1f0)
          (f32vector-set! bv  6  1f0)
          (f32vector-set! bv  7  1f0)
          (f32vector-set! bv  8  0f0)
          (f32vector-set! bv  9  1f0)
          (f32vector-set! bv 10  1f0)
          (f32vector-set! bv 11  1f0)
          (f32vector-set! bv 12  0f0)
          (f32vector-set! bv 13  0f0)
          (f32vector-set! bv 14  1f0)
          (f32vector-set! bv 15  0f0)
          (gl-bind-vertex-array vertex-array)
          (gl-bind-buffer target vertex-buffer)
          (gl-buffer-data target (bytevector-length bv)
                          (bytevector->pointer bv)
                          (arb-vertex-buffer-object static-draw-arb))
          (gl-vertex-attrib-pointer position 2 'float #f 0 %null-pointer)
          (gl-vertex-attrib-pointer texposition 2 'float #f 0
                                    (make-pointer (* 4 2 (sizeof float))))
          (gl-enable-vertex-attrib-array position)
          (gl-enable-vertex-attrib-array texposition))
        (close-render))
      (values render load-texture close-render))))

(define canvas-box (make-atomic-box (const #f)))

(define-public canvas-width #f)
(define-public canvas-height #f)
(define-public destroy-canvas #f)
(define-public render-canvas
  (let* ((width 2)
         (height 2)
         (bv (make-bytevector (* width height 4)))
         (surf (cairo-image-surface-create-for-data bv 'argb32
                                                    width height))
         (cr (cairo-create surf))
         (proc (const #f))
         (result (atomic-box-ref canvas-box)))
    (set! destroy-canvas (lambda ()
                           (when cr
                             (cairo-destroy cr)
                             (set! cr #f))
                           (when surf
                             (cairo-surface-destroy surf)
                             (set! surf #f))
                           (set! width 2)
                           (set! height 2)))
    (set! canvas-width (lambda () width))
    (set! canvas-height (lambda () height))
    (lambda (load-texture w h)
      (define (call-render-proc)
        (catch #t
          (lambda ()
            (call-with-values
                (lambda () (proc cr))
              (lambda results
                (lambda () (apply values results)))))
          (lambda args
            (set! proc (const #t))
            (lambda () (apply throw args)))))
      (unless (and (= w width) (= h height))
        (destroy-canvas)
        (set! width w)
        (set! height h)
        (set! bv (make-bytevector (* width height 4)))
        (set! surf (cairo-image-surface-create-for-data bv 'argb32
                                                        width height))
        (set! cr (cairo-create surf)))
      (let ((val (atomic-box-ref canvas-box)))
        (if (eq? val result)
            (call-render-proc)
            (begin
              (set! proc val)
              (set! result (call-render-proc))
              (atomic-box-set! canvas-box result))))
      (load-texture (bytevector->pointer bv) width height)
      (bytevector-fill! bv 0))))

(define render-thread #f)

(define-public (call-with-render-thread proc)
  (when (or (not (thread? render-thread))
            (thread-exited? render-thread))
    (set! render-thread (call-with-new-thread proc))))

(define-public (draw* proc)
  (unless render-thread
    (error "VIC is not running"))
  (atomic-box-set! canvas-box proc)
  (let lp ((result proc))
    (if (eq? result proc)
        (lp (atomic-box-ref canvas-box))
        (result))))

(define-public update-viewport
  (let ((width 2)
        (height 2))
    (lambda (w h)
      (unless (and (= w width) (= h height))
        (set! width w)
        (set! height h)
        (gl-viewport 0 0 width height)))))
