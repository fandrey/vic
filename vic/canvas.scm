;;; Copyright (C) 2021 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of VIC.
;;;
;;; VIC is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; VIC is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with VIC.  If not, see <http://www.gnu.org/licenses/>.

(define-module (vic canvas)
  #:use-module (cairo)
  #:use-module (vic render)
  #:export (draw
            call-with-save
            with-save)
  #:re-export (canvas-width
               canvas-height))

(define-public current-context (make-parameter #f))

(define-syntax-rule (draw form form* ...)
  (draw* (lambda (ctx)
           (parameterize ((current-context ctx))
             form form* ...))))

(define-syntax-rule (define-canvas-proc name cairo-proc args ...)
  (define-public (name args ...)
    (cairo-proc (current-context) args ...)))

(define-canvas-proc save cairo-save)
(define-canvas-proc restore cairo-restore)

(define (call-with-save proc)
  (dynamic-wind save proc restore))

(define-syntax-rule (with-save form form* ...)
  (call-with-save (lambda () form form* ...)))

(define-canvas-proc set-operator cairo-set-operator op)
(define-canvas-proc set-source-rgb cairo-set-source-rgb r g b)
(define-canvas-proc set-source-rgba cairo-set-source-rgba r g b a)
(define-canvas-proc set-line-width cairo-set-line-width width)
(define-canvas-proc set-line-cap cairo-set-line-cap line-cap)

(define-canvas-proc translate cairo-translate tx ty)
(define-canvas-proc scale cairo-scale sx sy)
(define-canvas-proc rotate cairo-rotate angle)
(define-canvas-proc paint cairo-paint)
(define-canvas-proc move-to cairo-move-to x y)
(define-canvas-proc new-sub-path cairo-new-sub-path)
(define-canvas-proc line-to cairo-line-to x y)
(define-canvas-proc curve-to cairo-curve-to x1 y1 x2 y2 x3 y3)
(define-canvas-proc arc cairo-arc xc yc radius angle1 angle2)

(define-canvas-proc stroke cairo-stroke)
(define-canvas-proc fill cairo-fill)

(define-canvas-proc set-font-size cairo-set-font-size size)
(define-canvas-proc show-text cairo-show-text utf8)
