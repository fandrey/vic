;;; Copyright (C) 2020 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of VIC.
;;;
;;; VIC is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; VIC is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with VIC.  If not, see <http://www.gnu.org/licenses/>.

(define-module (vic gl)
  #:use-module ((system foreign) #:select (sizeof
                                           uintptr_t
                                           bytevector->pointer
                                           %null-pointer
                                           pointer-address))
  #:use-module (rnrs bytevectors)
  #:use-module (gl types)
  #:use-module (gl enums)
  #:use-module (gl runtime)
  #:use-module ((gl low-level) #:renamer (symbol-prefix-proc '%)))

(define uintptr-vector
  (cond
   ((= (sizeof uintptr_t) 4) u32vector)
   ((= (sizeof uintptr_t) 8) u64vector)
   (else (error "wrong size of uintptr_t" (sizeof '*)))))

(define-public gl-bgra #x80e1)

(define glint-vector s32vector)
(define gluint-vector u32vector)
(define gluint-vector-ref u32vector-ref)
(define gluint-vector-set! u32vector-set!)

(define (check-gl-error subr)
  (let ((e (%glGetError))
        (msg "GL error ~A"))
    (if (= e (error-code invalid-enum))
        (scm-error 'gl-error subr msg 'invalid-enum #f))
    (if (= e (error-code invalid-value))
        (scm-error 'gl-error subr msg 'invalid-value #f))
    (if (= e (error-code invalid-operation))
        (scm-error 'gl-error subr msg 'invalid-operation #f))
    (if (= e (error-code stack-overflow))
        (scm-error 'gl-error subr msg 'stack-overflow #f))
    (if (= e (error-code stack-underflow))
        (scm-error 'gl-error subr msg 'stack-underflow #f))
    (if (= e (error-code table-too-large-ext))
        (scm-error 'gl-error subr msg 'table-too-large-ext #f))
    (if (= e (error-code texture-too-large-ext))
        (scm-error 'gl-error subr msg 'texture-too-large-ext #f))
    (unless (= e (error-code no-error))
      (scm-error 'gl-error subr "unknown error-code ~A" e #f))))

(define-syntax-rule (with-check-gl-error (name args ...))
  (let ((result (name args ...)))
    (check-gl-error (symbol->string 'name))
    result))

(export with-check-gl-error)

(define (make-gl-get type)
  (let ((get-proc (case type
                    ((shader) %glGetShaderiv)
                    ((program) %glGetProgramiv))))
    (lambda (obj pvalue)
      (let* ((bv (gluint-vector 0))
             (r (begin (get-proc obj pvalue (bytevector->pointer bv))
                       (gluint-vector-ref bv 0))))
        (if (or (= pvalue
                   (version-2-0 delete-status))
                (= pvalue
                   (version-2-0 compile-status))
                (= pvalue
                   (version-2-0 link-status)))
            (= r (boolean true))
            r)))))

(define %gl-get-shader (make-gl-get 'shader))
(define %gl-get-program (make-gl-get 'program))

(define-syntax-rule (gl-get-shader shader pname)
  (%gl-get-shader shader (version-2-0 pname)))

(define-syntax-rule (gl-get-program program pname)
  (%gl-get-program program (version-2-0 pname)))

(export gl-get-shader gl-get-program)

(define (%gl-get-info-log proc obj)
  (let ((bv-length (make-bytevector (sizeof (GLsizei))))
        (bv-info (make-bytevector 1024 0)))
    (proc obj
          (1- (bytevector-length bv-info))
          (bytevector->pointer bv-length)
          (bytevector->pointer bv-info))
    (utf8->string bv-info)))

(define-public (gl-get-shader-info-log shader)
  (%gl-get-info-log %glGetShaderInfoLog shader))

(define-public (gl-get-program-info-log program)
  (%gl-get-info-log %glGetProgramInfoLog program))

(define-public (gl-create-shader type)
  (let ((id (%glCreateShader
             (case type
               ((fragment) (version-2-0 fragment-shader))
               ((vertex) (version-2-0 vertex-shader))
               (else (error "wrong shader type" type))))))
    (if (= id 0)
        (error "failed to create shader" type)
        id)))

(define-public (gl-shader-source shader source)
  (let ((bv (string->utf8 source)))
    (with-check-gl-error
     (%glShaderSource shader
                      1
                      (bytevector->pointer
                       (uintptr-vector
                        (pointer-address
                         (bytevector->pointer bv))))
                      (bytevector->pointer
                       (glint-vector (bytevector-length bv)))))))

(define (bytevector->gluints bv)
  (let ((len (/ (bytevector-length bv) (sizeof (GLuint)))))
    (let lp ((i 0)
             (gluints (list)))
      (if (< i len)
          (lp (1+ i)
              (cons (gluint-vector-ref bv i)
                    gluints))
          gluints))))

(define-public (gl-get-attached-shaders program)
  (let* ((max-shaders (gl-get-program program attached-shaders))
         (bv-shaders (make-bytevector (* max-shaders (sizeof (GLuint))))))
    (with-check-gl-error
     (%glGetAttachedShaders program
                            max-shaders
                            %null-pointer
                            (bytevector->pointer bv-shaders)))
    (bytevector->gluints bv-shaders)))

(define-gl-procedure (glGenVertexArrays (n GLsizei)
                                        (arrays GLuint-*)
                                        -> void)
  "Generate N vertex arrays.")

(define-gl-procedure (glDeleteVertexArrays (n GLsizei)
                                           (arrays GLuint-*)
                                           -> void)
  "Delete vertex array objects.")

(define-gl-procedure (glBindVertexArray (array GLuint)
                                        -> void)
  "Bind vertex array object ARRAY.")

(export (glBindVertexArray . gl-bind-vertex-array))

(define-public (gl-gen-vertex-arrays n)
  (let ((bv (make-bytevector (* n (sizeof (GLuint))))))
    (with-check-gl-error (glGenVertexArrays n (bytevector->pointer bv)))
    (bytevector->gluints bv)))

(define-public (gl-delete-vertex-arrays arrays)
  (with-check-gl-error
   (glDeleteVertexArrays (length arrays) (apply gluint-vector arrays))))

(define-public (gl-delete-textures textures)
  (with-check-gl-error
   (%glDeleteTextures (length textures) (apply gluint-vector textures))))

(define (gl-data-type type)
  (case type
    ((byte) (data-type byte))
    ((unsigned-byte) (data-type unsigned-byte))
    ((short) (data-type short))
    ((unsigned-short) (data-type unsigned-short))
    ((int) (data-type int))
    ((unsigned-int) (data-type unsigned-int))
    ((float) (data-type float))
    ((double) (data-type double))
    (else (error "wrong GL data type ~A" type))))

(define-public (gl-vertex-attrib-pointer index size type
                                         normalized stride pointer)
  (with-check-gl-error
   (%glVertexAttribPointer index size (gl-data-type type)
                           (if normalized (boolean true) (boolean false))
                           stride pointer)))

(define-public (gl-tex-parameter target pname param)
  (with-check-gl-error (%glTexParameteri target pname param)))

(re-export (%glCompileShader . gl-compile-shader)
           (%glCreateProgram . gl-create-program)
           (%glAttachShader . gl-attach-shader)
           (%glLinkProgram . gl-link-program)
           (%glDeleteShader . gl-delete-shader)
           (%glDeleteProgram . gl-delete-program)
           (%glGetAttribLocation . gl-get-attrib-location)
           (%glUseProgram . gl-use-program)
           (%glTexImage2D . gl-tex-image-2d)
           (%glUniformMatrix3fv . gl-uniform-matrix-3fv)
           (%glGetUniformLocation . gl-get-uniform-location)
           (%glEnableVertexAttribArray . gl-enable-vertex-attrib-array)
           (%glBufferData . gl-buffer-data)
           current-gl-resolver)
