;;; Copyright (C) 2020 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of VIC.
;;;
;;; VIC is free software: you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; VIC is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with VIC.  If not, see <http://www.gnu.org/licenses/>.

(define-module (vic)
  #:use-module (ice-9 atomic)
  #:use-module (ice-9 threads)
  #:use-module (ice-9 receive)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:use-module (gl)
  #:use-module (glfw)
  #:use-module (cairo)
  #:use-module (vic render))

(define-public (vic)
  (call-with-render-thread
   (lambda ()
     (call-with-init
      (lambda ()
        (set-error-callback! (lambda (code string)
                               (error "GLFW error: ~A ~A" code string)))
        (call-with-window (create-window 1024 768 "VIC")
          (lambda (window)
            (make-context-current! window)
            (set-gl-clear-color .0 .0 .0 1.)
            (receive (render load-texture close-render) (init-render)
              (dynamic-wind
                (const #t)
                (lambda ()
                  (let loop ((quit (window-should-close? window)))
                    (unless quit
                      (receive (width height) (get-window-size window)
                        (update-viewport width height)
                        (render-canvas load-texture width height)
                        (render window)
                        (poll-events)
                        (loop (window-should-close? window))))))
                (lambda ()
                  (close-render)
                  (destroy-canvas)))))))))))

(unless (resolve-module '(geiser) #f #:ensure #f)
  (let ((m (resolve-module '(vic))))
    (set-current-module m)
    (vic)
    (let lp ((obj (read)))
      (unless (eof-object? obj)
        (eval obj m)
        (lp (read))))))
